﻿using UnityEngine;
using System.Collections;

public class BuildingScript : MonoBehaviour {

	protected BuildingsMenu menuScript;
    protected GameObject coreTower;

    protected float lastPositionX = Input.mousePosition.x;
    protected float lastPositionY = Input.mousePosition.y;
    protected bool activated;
    protected bool noCollision = true;
    protected int collisionNumber = 0;
    // Use this for initialization
    protected void Start () {
        gameObject.transform.parent = GameObject.Find("Tower View").transform;
		menuScript = GameObject.Find ("Main Camera").GetComponent<BuildingsMenu> ();
		coreTower = GameObject.Find("Core Tower");
	}

    // Update is called once per frame
    protected void Update () {
		if (activated) {
			if(Input.GetMouseButtonDown(0)){
				if(noCollision)
					Deactivate();
			}
			else
				Rotation();
		}
	}

    protected virtual void Rotation()
	{
		if (Input.GetMouseButton (2)) {
			transform.RotateAround (new Vector3 (0f, 0f, 0f), transform.up, (lastPositionX - Input.mousePosition.x) * 0.3f);
			lastPositionX = Input.mousePosition.x;
			transform.Translate (0f, (lastPositionY - Input.mousePosition.y) * -0.001f, 0f);
			lastPositionY = Input.mousePosition.y;
		} else {
			lastPositionX = Input.mousePosition.x;
			lastPositionY = Input.mousePosition.y;
		}
		if (gameObject.transform.position.y + gameObject.transform.lossyScale.y * 0.51f > 2*coreTower.transform.lossyScale.y)
			gameObject.transform.position = new Vector3 (gameObject.transform.position.x, 2*coreTower.transform.lossyScale.y - gameObject.transform.lossyScale.y * 0.51f, gameObject.transform.position.z);
		else if(gameObject.transform.position.y - gameObject.transform.lossyScale.y * 0.51f < 0f)
			gameObject.transform.position = new Vector3 (gameObject.transform.position.x, gameObject.transform.lossyScale.y * 0.51f, gameObject.transform.position.z);
	}

    public void Activate()
	{
        activated = true;
        GameObject.FindGameObjectWithTag("Menu").GetComponent<Canvas>().enabled=false;
        gameObject.layer = 2;
	}
    public void Deactivate()
	{
		activated = false;
        GameObject.FindGameObjectWithTag("Menu").GetComponent<Canvas>().enabled = true;
        gameObject.layer = 0;
    }
    virtual protected void OnTriggerEnter ()
	{
		collisionNumber++;
        if(collisionNumber>1){
            noCollision = false;
            gameObject.GetComponent<Renderer>().material.color = Color.red;
        }
		
	}
    virtual protected void OnTriggerExit()
	{
		collisionNumber--;
		if (collisionNumber == 1) {
			noCollision = true;
			gameObject.GetComponent<Renderer> ().material.color = Color.white;
		}
	}
}
