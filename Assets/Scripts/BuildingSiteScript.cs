﻿using UnityEngine;
using System.Collections;

public class BuildingSiteScript : BuildingScript
{
    override protected void Rotation()
    {
        if (!Input.GetMouseButton(1)){
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            Debug.DrawRay(ray.origin,ray.direction, Color.red);
            if (Physics.Raycast(ray.origin, ray.direction, out hit,9))
            {
                gameObject.transform.position = new Vector3(hit.point.x, 0.5f, hit.point.z);
            }
        }
    }
}
