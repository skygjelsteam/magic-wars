﻿using UnityEngine;

public class BuildingsMenu : MonoBehaviour {
    public GameObject firstBuilding;
    public GameObject secondBuilding;
    public GameObject firstBuildingSite;
    public GameObject secondBuildingSite;

    public void Building1(){
        GameObject createdBuilding = Instantiate(firstBuilding);
        createdBuilding.transform.position = new Vector3(0f, 1f, 0.68f);
        createdBuilding.GetComponent<BuildingScript>().Activate();
    }

    public void Building2(){
        GameObject createdBuilding = Instantiate(secondBuilding);
        createdBuilding.transform.position = new Vector3(0f, 1f, 0.6f);
        createdBuilding.GetComponent<BuildingScript>().Activate();
    }

    public void BuildingSite1(){
        GameObject createdBuilding = Instantiate(firstBuildingSite);
        createdBuilding.transform.position = new Vector3(0f, 0f, 0f);
        createdBuilding.GetComponent<BuildingSiteScript>().Activate();
    }
    public void BuildingSite2() {
        GameObject createdBuilding = Instantiate(secondBuildingSite);
        createdBuilding.transform.position = new Vector3(0f, 0f, 0f);
        createdBuilding.GetComponent<BuildingSiteScript>().Activate();
    }
    
}
