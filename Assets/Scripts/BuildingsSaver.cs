﻿using UnityEngine;
using System.Collections.Generic;

public class BuildingsSaver : Object {
    List<BuildingRepresentation> lBuildings;

    public void AddToMemory(GameObject toMemory)
    {
        lBuildings.Add(new BuildingRepresentation(toMemory));
    }

    public void AddToMemory(GameObject[] toMemory)
    {
        foreach (GameObject building in toMemory)
            AddToMemory(building);
    }

    public BuildingsSaver()
    {
        lBuildings = new List<BuildingRepresentation>();
    }

    public void RecreateBuildings()
    {
        foreach(BuildingRepresentation singleRepresentation in lBuildings)
        {
            singleRepresentation.Recreate();
        }
        lBuildings.Clear();
    }

    private class BuildingRepresentation : Object{
        private Vector3 location;
        private Quaternion rotation;
        private string nameOfPrefab;

        public GameObject Recreate()
        {
            return (GameObject)Instantiate(Resources.Load<GameObject>(nameOfPrefab), location, rotation);
        }
        public BuildingRepresentation(GameObject toMemory)
        {
            location = toMemory.transform.position;
            rotation = toMemory.transform.rotation;
            nameOfPrefab = toMemory.name.Replace("(Clone)", "");
        }
    }
}
