﻿using UnityEngine;
using System.Collections;

public class ButtonScript : MonoBehaviour {

	public void ToggleMe()
    {
        gameObject.SetActive(!gameObject.activeSelf);
    }
    public void HideMe()
    {
        gameObject.SetActive(false);
    }
}
