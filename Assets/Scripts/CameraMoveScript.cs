﻿using UnityEngine;
using System.Collections;

public class CameraMoveScript : MonoBehaviour {
	float startPositionX;
	float startPositionY;
	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButtonDown (1)) {
			startPositionX = Input.mousePosition.x;
			startPositionY = Input.mousePosition.y;
		}
		if (Input.GetMouseButton (1)) {
			transform.RotateAround (new Vector3(0f,transform.position.y,0f), GameObject.Find("Core Tower").transform.up , (startPositionX - Input.mousePosition.x) * 0.3f);
			startPositionX = Input.mousePosition.x;
			transform.Translate(0f,(startPositionY - Input.mousePosition.y)*-0.005f,0f);
			startPositionY = Input.mousePosition.y;
		}
	}
}
