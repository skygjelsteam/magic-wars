﻿using UnityEngine;
using System.Collections;

public class ChangeMenu : MonoBehaviour
{
    public GameObject myParent;

    public void Change(GameObject sce)
    {
        sce.SetActive(true);
        myParent.SetActive(false);
    }
}
