﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class MageManagmentScript : MonoBehaviour {

    public GameObject scrollprefab;
    public GameObject textprefab;
    public GameObject arrowbuttonprefab;
    public GameObject handle_to_main_game_object;
    public GameObject handle;
<<<<<<< HEAD
    public GameObject handle_to_canvas;
    public List<GameObject> handle_to_number_of_mages_text;
    public GameObject handle_to_scrollfield;
    public GameObject temp_mage_list;

	// Use this for initialization
	void Start () 
    {
        SetStandardText();
        SetScrollField();
        SetMageText();
    }
    void CreateScroll()
    {
        handle = Instantiate(scrollprefab);
        handle.transform.SetParent(handle_to_canvas.transform, true);
        handle.GetComponent<RectTransform>().anchorMin = new Vector2(1, 1);
        handle.GetComponent<RectTransform>().anchorMax = new Vector2(1, 1);
        handle.transform.localScale = new Vector3(1, 1, 1);
        handle.GetComponent<RectTransform>().rotation = handle_to_canvas.GetComponent<RectTransform>().rotation;
        handle.GetComponent<RectTransform>().localPosition = new Vector3(handle_to_canvas.GetComponent<RectTransform>().sizeDelta.x / 2 - handle.GetComponent<RectTransform>().sizeDelta.x / 2, handle_to_canvas.GetComponent<RectTransform>().sizeDelta.y / 2 - handle.GetComponent<RectTransform>().sizeDelta.y / 2, 0);
    }
=======
    public GameObject myChild;

	// Use this for initialization
	void Start () {
        handle=Instantiate(scrollpref);
        handle.transform.SetParent(myChild.transform,true);
        handle.GetComponent<RectTransform>().anchorMin = new Vector2(1, 1);
        handle.GetComponent<RectTransform>().anchorMax = new Vector2(1, 1);
        handle.transform.localScale = new Vector3(1, 1, 1);
        handle.GetComponent<RectTransform>().rotation = myChild.GetComponent<RectTransform>().rotation;
        handle.GetComponent<RectTransform>().localPosition = new Vector3(myChild.GetComponent<RectTransform>().sizeDelta.x / 2 - handle.GetComponent<RectTransform>().sizeDelta.x / 2, myChild.GetComponent<RectTransform>().sizeDelta.y / 2 - handle.GetComponent<RectTransform>().sizeDelta.y / 2, 0);
	}
>>>>>>> ccd28a503af49c0680bc7444e984e659d9091324

    public void SetStandardText()
    {
        handle = Instantiate(textprefab);
        handle.transform.SetParent(handle_to_canvas.transform, true);
        handle.GetComponent<RectTransform>().anchorMin = new Vector2(0, 1);
        handle.GetComponent<RectTransform>().anchorMax = new Vector2(0, 1);
        handle.transform.localScale = new Vector3(1, 1, 1);
        handle.GetComponent<RectTransform>().rotation = handle_to_canvas.GetComponent<RectTransform>().rotation;
        handle.GetComponent<RectTransform>().sizeDelta = new Vector2(handle_to_canvas.GetComponent<RectTransform>().sizeDelta.x / 2, handle_to_canvas.GetComponent<RectTransform>().sizeDelta.y / 7);
        handle.GetComponent<RectTransform>().localPosition = new Vector3(-handle_to_canvas.GetComponent<RectTransform>().sizeDelta.x * 0.2f, handle_to_canvas.GetComponent<RectTransform>().sizeDelta.y * 0.35f, 0);
        handle.GetComponent<Text>().text = "Type of Mage";


        handle = Instantiate(textprefab);
        handle.transform.SetParent(handle_to_canvas.transform, true);
        handle.GetComponent<RectTransform>().anchorMin = new Vector2(0, 1);
        handle.GetComponent<RectTransform>().anchorMax = new Vector2(0, 1);
        handle.transform.localScale = new Vector3(1, 1, 1);
        handle.GetComponent<RectTransform>().rotation = handle_to_canvas.GetComponent<RectTransform>().rotation;
        handle.GetComponent<RectTransform>().sizeDelta = new Vector2(handle_to_canvas.GetComponent<RectTransform>().sizeDelta.x * 0.4f, handle_to_canvas.GetComponent<RectTransform>().sizeDelta.y / 7);
        handle.GetComponent<RectTransform>().localPosition = new Vector3(-handle_to_canvas.GetComponent<RectTransform>().sizeDelta.x * (-0.25f), handle_to_canvas.GetComponent<RectTransform>().sizeDelta.y * 0.35f, 0);
        handle.GetComponent<Text>().text = "Amount";
    }


    public void SetScrollField()
    {
        handle_to_scrollfield.GetComponent<RectTransform>().anchorMin = new Vector2(0, 1);
        handle_to_scrollfield.GetComponent<RectTransform>().anchorMax = new Vector2(0, 1);
        handle_to_scrollfield.transform.localScale = new Vector3(1, 1, 1);
        handle_to_scrollfield.GetComponent<RectTransform>().rotation = handle_to_canvas.GetComponent<RectTransform>().rotation;
        handle_to_scrollfield.GetComponent<RectTransform>().sizeDelta = new Vector2(handle_to_canvas.GetComponent<RectTransform>().sizeDelta.x * 0.9f, handle_to_canvas.GetComponent<RectTransform>().sizeDelta.y * 5 / 7);
        handle_to_scrollfield.GetComponent<RectTransform>().localPosition = new Vector3(0, handle_to_canvas.GetComponent<RectTransform>().sizeDelta.y * (-0.1f));
    }

    public void SetMageText()
    {
        temp_mage_list = new GameObject();
        temp_mage_list.AddComponent<RectTransform>();
        temp_mage_list.transform.SetParent(handle_to_scrollfield.transform, true);
        temp_mage_list.GetComponent<RectTransform>().anchorMin = new Vector2(0, 1);
        temp_mage_list.GetComponent<RectTransform>().anchorMax = new Vector2(0, 1);
        temp_mage_list.transform.localScale = new Vector3(1, 1, 1);
        temp_mage_list.GetComponent<RectTransform>().rotation = handle_to_scrollfield.GetComponent<RectTransform>().rotation;
        temp_mage_list.GetComponent<RectTransform>().sizeDelta = new Vector2(handle_to_scrollfield.GetComponent<RectTransform>().sizeDelta.x, 0);
        temp_mage_list.GetComponent<RectTransform>().localPosition = new Vector3(temp_mage_list.GetComponent<RectTransform>().sizeDelta.x / 2, temp_mage_list.GetComponent<RectTransform>().sizeDelta.y / 2, 0);
        handle_to_scrollfield.GetComponent<ScrollRect>().content = temp_mage_list.GetComponent<RectTransform>();


        GameObject handle_to_actualplayer = handle_to_main_game_object.GetComponent<GameScript>().activ_player;
        int i = 0;
        foreach( MageType handle_to_list in handle_to_actualplayer.GetComponent<Splayer>().mage_type_list)
        {
            handle_to_number_of_mages_text.Add(Instantiate(textprefab));
            handle_to_number_of_mages_text[i].transform.SetParent(temp_mage_list.transform, true);
            handle_to_number_of_mages_text[i].GetComponent<RectTransform>().anchorMin = new Vector2(0, 1);
            handle_to_number_of_mages_text[i].GetComponent<RectTransform>().anchorMax = new Vector2(0, 1);
            handle_to_number_of_mages_text[i].transform.localScale = new Vector3(1, 1, 1);
            handle_to_number_of_mages_text[i].GetComponent<RectTransform>().rotation = handle_to_scrollfield.GetComponent<RectTransform>().rotation;
            handle_to_number_of_mages_text[i].GetComponent<RectTransform>().sizeDelta = new Vector2(handle_to_canvas.GetComponent<RectTransform>().sizeDelta.x * 0.4f, handle_to_canvas.GetComponent<RectTransform>().sizeDelta.y / 7);
            handle_to_number_of_mages_text[i].GetComponent<Text>().text = handle_to_actualplayer.GetComponent<Splayer>().mage_type_list[i].number_of_mages_in_type.ToString();


            handle = Instantiate(textprefab);
            handle.transform.SetParent(temp_mage_list.transform, true);
            handle.GetComponent<RectTransform>().anchorMin = new Vector2(0, 1);
            handle.GetComponent<RectTransform>().anchorMax = new Vector2(0, 1);
            handle.transform.localScale = new Vector3(1, 1, 1);
            handle.GetComponent<RectTransform>().rotation = handle_to_canvas.GetComponent<RectTransform>().rotation;
            handle.GetComponent<RectTransform>().sizeDelta = new Vector2(handle_to_canvas.GetComponent<RectTransform>().sizeDelta.x * 0.5f, handle_to_canvas.GetComponent<RectTransform>().sizeDelta.y / 7);
            handle.GetComponent<RectTransform>().localPosition = new Vector3(handle_to_number_of_mages_text[i].GetComponent<RectTransform>().sizeDelta.x * -1/ 2, handle_to_number_of_mages_text[i].GetComponent<RectTransform>().sizeDelta.y / 2 - handle_to_number_of_mages_text[i].GetComponent<RectTransform>().sizeDelta.y * i, 0);
            handle.GetComponent<Text>().text = handle_to_list.Type;

            handle_to_number_of_mages_text[i].GetComponent<RectTransform>().localPosition = new Vector3(handle_to_number_of_mages_text[i].GetComponent<RectTransform>().sizeDelta.x * -1 / 2 + handle.GetComponent<RectTransform>().sizeDelta.x, handle_to_number_of_mages_text[i].GetComponent<RectTransform>().sizeDelta.y / 2 - handle_to_number_of_mages_text[i].GetComponent<RectTransform>().sizeDelta.y * i, 0);
            

            i++;
        }

        temp_mage_list.GetComponent<RectTransform>().sizeDelta = new Vector2(handle_to_scrollfield.GetComponent<RectTransform>().sizeDelta.x, handle_to_canvas.GetComponent<RectTransform>().sizeDelta.y * i / 7);
        temp_mage_list.GetComponent<RectTransform>().localPosition = new Vector3(temp_mage_list.GetComponent<RectTransform>().sizeDelta.x / 2, temp_mage_list.GetComponent<RectTransform>().sizeDelta.y / 2, 0);
    }
}
