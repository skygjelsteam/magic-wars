﻿using UnityEngine;
using System.Collections;

public class MageType{

    public string Type;
    private int chaosdepravation;
    private int acceptation;
    public int number_of_mages_in_type;
    public int index_of_type;

    public MageType(string role, int index_of_type)
    {
        this.Type = role;
        chaosdepravation = 0;
        acceptation = 50;
        number_of_mages_in_type = 0;
        this.index_of_type = index_of_type;
    }

    public void AddMage(int number_of_added_mages)
    {
        number_of_mages_in_type += number_of_added_mages;
    }

    public int GiveNumberOfMages()
    {
        return number_of_mages_in_type;
    }
}
