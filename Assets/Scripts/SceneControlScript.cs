﻿using UnityEngine;
using System.Collections;

public class SceneControlScript : MonoBehaviour {

    public static bool mainscene = true;
    public GameObject objectMainscene;

	// Use this for initialization
	void Start () {
	
	}
    public void change()
    {
        mainscene = !mainscene;
    }
	
	// Update is called once per frame
	void Update () {
        if (mainscene == true && objectMainscene.activeSelf == false)
            objectMainscene.SetActive(true);
	}
}
