﻿using UnityEngine;
using System.Collections;

public class Sgame : MonoBehaviour
{
    const int NUMBER_OF_PLAYERS = 2;
    public Splayer[] players;
    public Squest[] allquest;
    public int turncounter;
    public Splayer activeplayer;

    void Start()
    {
        players = new Splayer[NUMBER_OF_PLAYERS];
        this.AddPlayers();
        turncounter = 0;
        activeplayer = players[0];
    }

    void AddPlayers()
    {
        for (int i = 0; i < NUMBER_OF_PLAYERS; i++)
        {
            players[i].Initiate("heh");
        }
    }
}