﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Splayer : MonoBehaviour
{

    public string fracname;
    public int gold;
    public int mithril;
    public List<MageType> mage_type_list;
    public int mage_growth;
    public int new_mage_timer;
    public int number_of_all_posesd_mages;

    public void Start()
    {
        Initiate("DEBIL HAHAHHAHAH");
    }
    
    public void Initiate(string Fracname)
    {
        fracname = Fracname;
        gold = 100;
        mithril = 10;
        mage_type_list= new List<MageType>();
        mage_type_list.Add(new MageType("Mage",0));
        mage_type_list.Add(new MageType("Battle Mage", 1));
        mage_type_list[0].AddMage(5);
        mage_type_list[1].AddMage(3);
        mage_growth = 1;
        number_of_all_posesd_mages = 11;
        new_mage_timer = 0;
    }

    public void SortMages()
    {
        mage_type_list.Sort((a, b) => a.index_of_type.CompareTo(b.index_of_type));
    }
}